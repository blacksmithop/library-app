const mongoose = require('mongoose');

const uri = `mongodb+srv://testuser:KTik3r6da2G43MaZ@cluster0.8flhu.mongodb.net/LibraryData`;

mongoose 
 .connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,   })   
 .catch(err => console.log(err));

const Schema = mongoose.Schema;


const AuthorModel = new Schema({
    name: String,
    country : String,
    imageLink : String,
    language : String,
    title: String
});


var AuthorData = mongoose.model('author',AuthorModel, 'author');


module.exports = AuthorData;