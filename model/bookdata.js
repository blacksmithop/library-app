const mongoose = require('mongoose');

const uri = `mongodb+srv://testuser:KTik3r6da2G43MaZ@cluster0.8flhu.mongodb.net/LibraryData`;

mongoose 
 .connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,   })   
 .then(() => console.log("Database connected!"))
 .catch(err => console.log(err));

const Schema = mongoose.Schema;


const BookModel = new Schema({
    name: String,
    author : String,
    genre : String,
    imageLink : String,
    about: String
});


var BookData = mongoose.model('book',BookModel, 'book');


module.exports = BookData;