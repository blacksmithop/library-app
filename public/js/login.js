$('input').blur(function() {
  validate();
});

validate = () => {
  let user = $('.user').val();
  let pwd = $('.pwd').val();
  if ((user == 'admin') && (pwd=='12345')){
    success();
  }
  else if (user == '') {
    failure(" Username is blank", ".username");
  }

  else if (user.length >= 5) {
    failure(" Username is valid", ".username");

    let str = passwordStrength(pwd);
    if (str == 3) {
      failure("Password is valid", ".password");
      success();
    }
    else {
      failure(" Password is not strong enough", ".password");
    }
  }
  else {
    failure("Minimum length: 5", ".username");

  }
  if (pwd == '') {
    failure(" Password is blank", ".password");
  }
}

success = () => {
  $('.login').attr('disabled', false);
  console.log("Form: Valid");
}

failure = (message, cls) => {
  $('.login').attr('disabled', true);
  $(cls).text(message);
}

$('.user').click(() => {
  $('.username').text('')
})

$('.pwd').click(() => {
  $('.password').text('')
})


let strong = /.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/; // alphanum and special characters
let weak = /[a-zA-Z]/;  // alphabets
let medium = /\d+/;
let mobilexp = /^(\d{3})(\.|-)?(\d{3})(\.|-)?(\d{4})$/;

function passwordStrength(pwd) {

  let str = 0;

  if (pwd.length <= 7 && (pwd.match(weak) ||
    pwd.match(medium) || pwd.match(strong))) {
    str = 1;
  }
  if (pwd.length >= 8 && ((pwd.match(weak) && pwd.match(medium)) ||
    (pwd.match(medium) && pwd.match(strong)) ||
    (pwd.match(weak) && pwd.match(strong)))) {
    str = 2;
  }
  if (pwd.length >= 10 && pwd.match(weak) &&
    pwd.match(medium) && pwd.match(strong)) {
    str = 3;
  }
  return str
}

