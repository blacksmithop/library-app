const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const BookData = require('../model/bookdata');


const fs = require('fs');
var book = express.Router();

book.use(bodyParser.urlencoded({ extended: true }));

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/img/book/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})

var upload = multer({ storage: storage })

book.get('/book', (req, res) => {
    BookData.find({}, function (err, books) {
        if (err) {
            console.log(err);
        }
        else if (books) {
            const bookArray = books;
            res.render('book', { bookArray });
        }
        else {
            console.log(123);
        }
    });
});

book.get('/book/id/:bookid?', (req, res) => {
    BookData.findById(req.params.bookid, function (err, book) {
        if (err) {
            console.log(err);
        }
        else if (book) {
            res.render('partials/singlebook', { book });
        }
        else {
            console.log(123);
        }
    });
});

book.get('/book/update/:bookid?', (req, res) => {
    BookData.findById(req.params.bookid, function (err, book) {
        if (err) {
            console.log(err);
        }
        else if (book) {
            res.render('updatebook', {book});
        }
        else {
            console.log(123);
        }
    });
});

book.post('/book/update/', upload.single('uploaded_file'), (req, res) => {

    const bookObject = {
        name : req.body.name,
        author : req.body.author,
        genre : req.body.genre,
        about : req.body.about}

    if (req.body.imageLink){
        bookObject.imageLink = req.file.originalname;
    }
    else{
        BookData.findById(req.body.id, function (err, book) {
            if (err) {
                console.log(err);
            }
            else if (book) {
                console.log(book);
                bookObject.imageLink = book.imageLink;
            }
            else {
                console.log(123);
            }
        });
    }
    
    BookData.findByIdAndDelete(req.body.id, function (err, author) {
        if (err) {
            console.log(err);
        }
        else if (author) {
            console.log("Deleted boook");
        }
        else {
            console.log(123);
        }
    });
    var bookadd = BookData(bookObject);

    bookadd.save().then(function (data) {
        console.log('Book added');
        return res.redirect('/book');

    }).catch(function (error) {
        console.log('error added', error);
    })
});


book.post('/book/update', (req, res) => {
    console.log(req.body);
});


book.get('/book/delete/:bookid?', (req, res) => {
    BookData.findByIdAndDelete(req.params.bookid, function (err, book) {
        if (err) {
            console.log(err);
        }
        else if (book) {
            console.log("Deleted book");
            console.log(book);
            res.redirect("/book");
        }
        else {
            console.log(123);
        }
    });
});

book.get('/book/delete/:bookid?', (req, res) => {
    BookData.findByIdAndDelete(req.params.bookid, function (err, book) {
        if (err) {
            console.log(err);
        }
        else if (book) {
            console.log("Deleted book");
            console.log(book);
            res.redirect("/book");
        }
        else {
            console.log(123);
        }
    });
});


book.get('/addbook', (req, res) => {
    if (req.session.role == "user") {
        res.redirect('/book');
    }
    else {
        res.render('addbook');
    }
})

book.post('/addbook', upload.single('uploaded_file'), (req, res) => {

    const bookObject = {
    name : req.body.name,
    author : req.body.author,
    genre : req.body.genre,
    imageLink : req.file.originalname,
    about : req.body.about}

    var bookadd = BookData(bookObject);

    bookadd.save().then(function (data) {
        console.log('Book added');
        return res.redirect('book');

    }).catch(function (error) {
        console.log('error added', error);
    })


    res.redirect('/book');

})


module.exports = book;