const express = require('express');
const bodyParser = require('body-parser');
const SignUpData = require('../model/signupdata');


var auth = express.Router();

auth.use(bodyParser.urlencoded({ extended: true }));

auth.get('/login', (req, res) => {
    res.render('login');
});


auth.post('/login', (req, res) => {
    if ((req.body.username == "admin") && (req.body.password == "12345")) {
        req.session.role = 'admin';
        res.redirect('/');
        return;
    }
    SignUpData.findOne({ username: req.body.username, password: req.body.password }, function (err, user) {
        if (err) {
            console.log(err);
        }
        else if (user) {
            console.log("User found")
            req.session.role = 'user';
            res.redirect('/');
        }
        else {
            res.render('login', { message: "Invalid credentials" })
        }
    });
});


auth.get('/signup', (req, res) => {
    res.render('signup');
});

auth.post('/signup', (req, res) => {
    let message = "";
    var exist = false;
    SignUpData.findOne({ username: req.body.username }, function (err, user) {
        if (err) {
            console.log(err);
        }
        else if (user) {
            console.log("User found")
            res.render('login', { message: "Username already registered" });
        }
        else {
            var signup = SignUpData({
                name: req.body.name,
                username: req.body.username,
                password: req.body.password
            });

            signup.save().then(function (data) {
                console.log('Data added');
                req.session.role = 'user';
                return res.redirect('login');

            }).catch(function (error) {
                console.log('error added', error);
            })
        }
    });


});




module.exports = auth;
