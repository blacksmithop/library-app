const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const AuthorData = require('../model/authordata');

var author = express.Router();

author.use(bodyParser.urlencoded({ extended: true }));

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/img/author/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})

var upload = multer({ storage: storage })

author.get('/author', (req, res) => {
    AuthorData.find({}, function (err, authors) {
        if (err) {
            console.log(err);
        }
        else if (authors) {
            const authArray = authors;
            res.render('author', { authArray });
        }
        else {
            console.log(123);
        }
    });
});

author.get('/author/id/:authorid?', (req, res) => {
    AuthorData.findById(req.params.authorid, function (err, author) {
        if (err) {
            console.log(err);
        }
        else if (author) {
            res.render('partials/singleauthor', { author });
        }
        else {
            console.log(123);
        }
    });
});


author.get('/author/update/:authorid?', (req, res) => {
    AuthorData.findById(req.params.authorid, function (err, author) {
        if (err) {
            console.log(err);
        }
        else if (author) {
            res.render('updateauthor', {author});
        }
        else {
            console.log(123);
        }
    });
});

author.post('/author/update/', upload.single('uploaded_file'), (req, res) => {

    const authorObject = {
        name : req.body.name,
        country : req.body.country,
        language : req.body.language,
        title : req.body.title};
        
    if (req.body.imageLink){
        authorObject.imageLink = req.file.originalname;
    }
    else{
        AuthorData.findById(req.body.id, function (err, author) {
            if (err) {
                console.log(err);
            }
            else if (author) {
                console.log(author);
                authorObject.imageLink = author.imageLink;
            }
            else {
                console.log(123);
            }
        });
    }
    
    AuthorData.findByIdAndDelete(req.body.id, function (err, author) {
        if (err) {
            console.log(err);
        }
        else if (author) {
            console.log("Deleted author");
        }
        else {
            console.log(123);
        }
    });
    var authoradd = AuthorData(authorObject);

    authoradd.save().then(function (data) {
        console.log('Author added');
        return res.redirect('/author');

    }).catch(function (error) {
        console.log('error added', error);
    })
});


author.get('/author/delete/:authorid?', (req, res) => {
    AuthorData.findByIdAndDelete(req.params.authorid, function (err, author) {
        if (err) {
            console.log(err);
        }
        else if (author) {
            console.log("Deleted author");
            console.log(author);
            res.redirect("/author");
        }
        else {
            console.log(123);
        }
    });
});

author.get('/addauthor', (req, res) => {
    if (req.session.role == "user") {
        res.redirect('/author');
    }
    else {
        res.render('addauthor');
    }
})

author.post('/addauthor', upload.single('uploaded_file'), (req, res) => {

    const authorObject = {
    name : req.body.name,
    country : req.body.country,
    language : req.body.language,
    imageLink : req.file.originalname,
    title : req.body.title};

    var authoradd = AuthorData(authorObject);

    authoradd.save().then(function (data) {
        console.log('Author added');
        return res.redirect('author');

    }).catch(function (error) {
        console.log('error added', error);
    })

})


module.exports = author;