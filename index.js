const express = require('express')
const auth = require('./routes/auth');
const author = require('./routes/author');
const book = require('./routes/book');

const app = express();
// session
const session = require('express-session');

app.use(session({      //session creation
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
}));


app.get('/logout', function (req, res) {
    req.session.destroy();
    res.redirect('login');
});

app.use(session({
    secret: 'supersecret password',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
}));


app.set('view engine', 'ejs');
app.set('views', './src/views')

app.use(express.static(__dirname + '/public'));

var index = express.Router();

// Log usage to console
index.use('/', (req, res, next) => {
    console.log(`Request: ${req.originalUrl} (${req.method})`)
    next()
})


index.get('/', (req, res) => {
    res.render('index', { res: res });
});



app.use(auth)

app.use(function(req, res, next) {  
    if( typeof req.session.role == "undefined") {       // requiring a valid access token
        res.redirect('/login');
    } else {
        next();
    }
});

app.use(index)

app.use(author)
app.use(book)

app.use(function(req, res, next) {
    res.locals.user = req.session.user;
    next();
  });

const port = (process.env.PORT || 3000)



app.listen(port, () => {
    console.log(`\nListening to port: ${port}\nLive at: http://localhost:${3000}/\n`);
});